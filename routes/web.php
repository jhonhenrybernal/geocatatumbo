<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//funciones quemadas
Route::get('/login-two', 'Auth\loginController@index')->name('login-two');
Route::post('/login-access', 'Auth\loginController@access')->name('login-access');
Route::get('/material-estudio', 'MaterialController@index')->name('material');
Route::get('/material-estudio-geometria', 'MaterialController@geometria')->name('geometria');

