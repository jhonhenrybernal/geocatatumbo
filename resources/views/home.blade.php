@extends('layouts.app')

@section('content')
<body background="img\acceso.jpg" class="login">
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Bienvenidos</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <a href="/material-estudio" class="button">MATERIAL DE ESTUDIO</a>
                     <a href="/calificaciones"  class="button">CALIFICACIONES</a>
                      <a href="/perfil" class="button">PERFIL</a>
                </div>
            </div>
        </div>
    </div>
</div>
<body>
@endsection
