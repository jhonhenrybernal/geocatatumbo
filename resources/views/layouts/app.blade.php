<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/book.ico" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Sistema estudiantil</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
      <style>
            

            .login {
                min-height: 690px;text-align: center;
                color: #231f1f;
                background-repeat: no-repeat;
                background-position: center;
                background-size: cover;
            }

            .button {
                background-color: #4CAF50;
                border: none;
                color: white;
                padding: 56px 65px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 16px 20px;
                cursor: pointer;
            }
            .button-desactivado {
                background-color: #324e34;
                border: none;
                color: white;
                padding: 56px 65px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 16px 20px;
                cursor: pointer;
            }

            .button-angulos {
                background-color: #e0865d;
                border: none;
                color: white;
                padding: 56px 65px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 16px 20px;
                cursor: pointer;
            }

            .button-geometricas {
                background-color: #324e34;
                border: none;
                color: white;
                padding: 56px 65px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 16px 20px;
                cursor: pointer;
            }

            .button-practicas {
                background-color: #ded35f;
                border: none;
                color: white;
                padding: 56px 65px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 16px 20px;
                cursor: pointer;
            }


        </style>
        <style>
        
        input[type=text], input[type=password] {
          width: 100%;
          padding: 12px 20px;
          margin: 8px 0;
          display: inline-block;
          border: 1px solid #ccc;
          box-sizing: border-box;
        }

        button {
          background-color: #4CAF50;
          color: white;
          padding: 14px 20px;
          margin: 8px 0;
          border: none;
          cursor: pointer;
          width: 100%;
        }

        button-desactivado {
          background-color: #4CAF50;
          color: white;
          padding: 14px 20px;
          margin: 8px 0;
          border: none;
          cursor: pointer;
          width: 100%;
        }

        button:hover {
          opacity: 0.8;
        }

        .cancelbtn {
          width: auto;
          padding: 10px 18px;
          background-color: #f44336;
        }

        .imgcontainer {
          text-align: center;
          margin: 24px 0 12px 0;
        }

        img.avatar {
          width: 40%;
          border-radius: 50%;
        }

        .container {
          padding: 0px;
        }

        span.psw {
          float: right;
          padding-top: 16px;
        }

        /* Change styles for span and cancel button on extra small screens */
        @media screen and (max-width: 300px) {
          span.psw {
             display: block;
             float: none;
          }
          .cancelbtn {
             width: 100%;
          }
        }
        </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                       Sistema estudiantil
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest() and $status)
                            <!--li><a href="{{ route('login-two') }}">Login</a></li-->
                            <!--li><a href="{{ route('register') }}">Register</a></li-->
                          
                        @else
                             <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Material de estudio
                                <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                  <li><a href="#">Geometria</a>
                                    
                                  </li>
                                </ul>
                              </li>
                               <li class="dropdown">
                               <a  href="/" type="submit" 
                                           >
                                            Calificaciones
                                        </a>
                               </li>
                                <li class="dropdown">
                               <a  href="/" type="submit" 
                                           >
                                            Perfil
                                        </a>
                               </li>  
                              <li class="dropdown">
                               <a  href="/" type="submit" 
                                           >
                                            Salir
                                        </a>
                               </li> 
                   
                            <!--li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <?php //Auth::user()->name ?> <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li-->
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
