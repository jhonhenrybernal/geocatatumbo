<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MaterialController extends Controller
{
    public function index(){
        $status = false;
        return view('material-estudio.index',compact('status'));
    }

    public function geometria(){
        $status = false;
        return view('material-estudio.geometria',compact('status'));
    }
}
